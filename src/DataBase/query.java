package DataBase;
import data.arbre;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class query {

    /**
     * @param File = le fichier CSV contenant les arbres, donné dans l'énoncé du projet
     * @return maListe = Une liste de tous les arbres contenus dans le fichier CSV
     * @throws IOException Les erreurs dues à la lecture du fichier
     */
    public static List<arbre> extract(String File) throws IOException {
        List<arbre> maListe = new ArrayList<>();
        BufferedReader csvReader = new BufferedReader(new FileReader(File));
        String row = csvReader.readLine();                                         // Passe la première ligne

        while ((row = csvReader.readLine()) != null)
        {
            String[] data = row.split(";");

            try
            {
                arbre e = new arbre(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8],
                        data[9], data[10], data[11], data[12], data[13], data[14], data[15], data[16]);

                maListe.add(e);
            }
            catch (ArrayIndexOutOfBoundsException ex)   // Traitement du cas où la ligne csv est sur deux lignes
            {                                           // En partant du principe que une ligne ne fait maximum 2
                row = csvReader.readLine();
                String[] a = row.split(";");

                // Complétion de la dernière ligne non finie
                data[data.length - 1] = data[data.length - 1] + a[0];

                // Création d'un nouveau tableau data avec les deux lignes
                String[] newData = new String[17];

                for (int i = 0; i < 17; i++)
                {
                    if (i < data.length)
                    {
                        newData[i] = data[i];
                    }
                    else
                    {
                        newData[i] = a[i - data.length];
                    }
                }

                // Création de l'arbre
                arbre e = new arbre(newData[0], newData[1], newData[2], newData[3], newData[4], newData[5], newData[6],
                        newData[7], newData[8], newData[9], newData[10], newData[11], newData[12], newData[13],
                        newData[14], newData[15], newData[16]);

                maListe.add(e);

            }
        }

        return maListe;
    }

    /**
     * Retourne une liste des arbres remarquables seulement, à partir d'une liste d'arbre
     * @param arbreList Une liste d'arbre
     * @return          La liste des arbres remarquables de arbbreList
     */
    public static List<arbre> SelectRemarquable(List<arbre> arbreList)
    {
        List<arbre> listRemarquable = new ArrayList<>();

        for (arbre a : arbreList)
        {
            if (a.isRemarquable())
            {
                listRemarquable.add(a);
            }
        }

        return listRemarquable;
    }

    /**
     * Retourne ule liste des arbres NON remarquables seulement, à partir d'une liste d'arbre
     * @param arbreList Une liste d'arbre
     * @return          La liste des arbres non remarquables de arbreList
     */
    public static List<arbre> SelectNonRemarquable(List<arbre> arbreList)
    {
        List<arbre> listNonRemarquable = new ArrayList<>();

        for (arbre a : arbreList)
        {
            if (!a.isRemarquable())
            {
                listNonRemarquable.add(a);
            }
        }

        return listNonRemarquable;
    }

    /**
     * Trie la liste arbreListNonTriee et retourne la liste triée par stade de développement décroissant
     * @param   arbreListNonTriee La liste non triée
     * @return  La liste triée par stade de développement décroissant
     */
    public static List<arbre> TriParAnciennete(List<arbre> arbreListNonTriee)
    {
        boolean trie = false;
        List<arbre> arbreList = new ArrayList<>(arbreListNonTriee);

        while (!trie)
        {
            trie = true;
            for(int i = 0; i < arbreList.size()-1; i++)
            {
                if (!arbre.compareAnciennete(arbreList.get(i), arbreList.get(i+1)))
                {
                    trie = false;

                    arbre temp = arbreList.get(i);
                    arbreList.set(i, arbreList.get(i+1));
                    arbreList.set(i+1, temp);
                }
            }
        }

        return arbreList;
    }

    /**
     * Tri une liste par nombre de voie décroissantes
     * @param   arbreListComplete La liste à trier
     * @return  La liste triée
     */
    public static List<arbre> TriParVoies(List<arbre> arbreListComplete) {
        boolean trie = false;
        List<arbre> arbreList = new ArrayList<>(arbreListComplete);


        while (!trie) {
            trie = true;
            for (int i = 0; i < arbreList.size() - 1; i++) {
                if (arbreList.get(i).getNbVoies() < arbreList.get(i + 1).getNbVoies()) {
                    trie = false;

                    arbre temp = arbreList.get(i);
                    arbreList.set(i, arbreList.get(i + 1));
                    arbreList.set(i + 1, temp);
                }
            }
        }
        return arbreList;
    }

    /**
     * Donne l'arbre de arbreList à l'identifiant ID
     * @param arbreList La liste dans laquelle on cherche
     * @param id        L'id de l'arbre que l'on cherche
     * @return          L'arbre de "arbreList" à l'ID "id"
     */
    public static arbre SelectArbreID(List<arbre> arbreList, int id)
    {
        for (data.arbre arbre : arbreList) {
            if (arbre.getID() == id)
                return arbre;
        }
        return null;
    }

    /**
     * Tri la liste par hauteur décroissante
     * @param   ListeNonTriee La liste non tirée
     * @return  La liste triée
     */
    public static List<arbre> TriParHauteur(List<arbre> ListeNonTriee) {
        boolean trie = false;
        List<arbre> arbreList = new ArrayList<>(ListeNonTriee);

        while (!trie) {
            trie = true;
            for (int i = 0; i < arbreList.size() - 1; i++) {
                if (arbreList.get(i).getHauteur() < arbreList.get(i + 1).getHauteur()) {
                    trie = false;

                    arbre temp = arbreList.get(i);
                    arbreList.set(i, arbreList.get(i + 1));
                    arbreList.set(i + 1, temp);
                }
            }
        }
        return arbreList;
    }

    /**
     * Tri la liste par circonférence décroissante
     * @param   ListeNonTriee La liste non tirée
     * @return  La liste triée
     */
    public static List<arbre> TriParCirconference(List<arbre> ListeNonTriee) {
        boolean trie = false;
        List<arbre> arbreList = new ArrayList<>(ListeNonTriee);

        while (!trie) {
            trie = true;
            for (int i = 0; i < arbreList.size() - 1; i++) {
                if (arbreList.get(i).getCirconference() < arbreList.get(i + 1).getCirconference()) {
                    trie = false;

                    arbre temp = arbreList.get(i);
                    arbreList.set(i, arbreList.get(i + 1));
                    arbreList.set(i + 1, temp);
                }
            }
        }
        return arbreList;
    }

    public static List<arbre> SelectArbreVote(List<arbre> arbreList) {
        List<arbre> output = new ArrayList<>();

        for (arbre a : arbreList)
        {
            if (a.getNbVoies() > 0)
            {
                output.add(a);
            }
        }

        return output;
    }
}
