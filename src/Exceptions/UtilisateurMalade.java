package Exceptions;

/**
 * Correspond aux erreurs de l'utilisateur, dû à un manque de compréhension ou à la COVID19
 */
public class UtilisateurMalade extends Exception{
    public UtilisateurMalade(String msg)
    {
        super(msg);
    }
}
