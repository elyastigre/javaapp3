package Exceptions;

public class MembreDoesntExist extends EtudiantsMalades{
    public MembreDoesntExist(int id)
    {
        super("Le membre suivant n'existe pas : " + id);
    }
}
