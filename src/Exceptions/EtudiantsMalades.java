package Exceptions;

/**
 * Erreurs que ne devraient pas voir les utilisateurs, dues au malfonctionnement d'une fonction, à cause d'une erreur de porgrammation (et de la COVID19)
 */
public class EtudiantsMalades extends Exception{
    public EtudiantsMalades(String msg)
    {
        super(msg);
    }
}
