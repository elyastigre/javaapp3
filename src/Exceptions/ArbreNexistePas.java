package Exceptions;

public class ArbreNexistePas extends Exception{
    public ArbreNexistePas(int id)
    {
        super("Cette arbre n'existe pas, ID : " + id);
    }
}
