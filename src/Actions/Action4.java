package Actions;

public class Action4 implements Action{
    public static void execute(){
        AfficheChoix();
        int choix = Integer.parseInt(util.LireClavier("Votre choix :"));
        switch (choix) {
            case 1 -> util.ajouteDonnateur();
            case 2 -> util.supprimeDonnateur();
            default -> {
            }
        }
    }

    public static void AfficheChoix() {
        System.out.println(
                """
                        Veuillez séléctionner une action :
                        1 : Ajout d'un donnateur
                        2 : Suppréssion d'un donnateur
                        0 : Revenir au menu principal
                        """
        );
    }
}
