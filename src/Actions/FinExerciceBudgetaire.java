package Actions;

import data.Budget;
import data.arbre;
import data.membre;

import java.util.List;

public class FinExerciceBudgetaire implements Action {
    public static void execute(List<arbre> arbreList)
    {
        util.RevocationSiNonCotisation();
        util.ConstitutionListeArbresRemarquables(arbreList);
        util.GenerationRapportActivite();
        util.EnvoiDemandeDons();

        membre.ResetAPaye();

        Budget.verifieGameOver();

        Budget.setCompteDebDeLAnnee();

        Budget.resetNbDeffrayement();

        if (!membre.idExiste(membre.getPresident()))
        {
            util.electionNouveauPresident();
        }

        System.out.println("Joyeuse Noel ! ");
    }
}
