package Actions;

import Exceptions.EtudiantsMalades;
import Exceptions.MembreDoesntExist;
import Exceptions.UtilisateurMalade;
import data.arbre;
import data.membre;

import java.util.List;

public class Action6 implements Action{

    /**
     * Execute l'action 6 : "Vote d’un membre en faveur de la reconnaissance d’un arbre remarquable."
     * @param arbreList La liste des arbres
     */
    public static void execute(List<arbre> arbreList)
    {
        membre Votant;
        try
        {
            int id;

            try
            {
                id = Integer.parseInt(util.LireClavier("Veuillez entrer votre ID : "));
            }
            catch (NumberFormatException e)
            {
                id = -12;
            }

            if (!membre.idExiste(id)) throw new MembreDoesntExist(id);

            Votant = membre.chercheMembre(id);
        }
        catch(MembreDoesntExist e)
        {
            System.out.println(e.getMessage());
            return;
        }

        List<arbre> nonRemarquableList = util.AfficheArbresNonRemarquables(arbreList);
        int choix = 0;
        boolean erreur = true;

        while (erreur)
        {
            erreur = false;
            try
            {
                choix = Integer.parseInt(util.LireClavier("Votre choix : "));
                if (choix >= nonRemarquableList.size() || choix < 0)
                {
                    throw new Exceptions.UtilisateurMalade("Choix non valide");
                }
            }
            catch (UtilisateurMalade e)
            {
                System.out.println("Choix non valide... Veuillez recommencer");
                erreur = true;
            }
        }

        try
        {
            Votant.Vote(arbreList, nonRemarquableList.get(choix).getID());
        }
        catch (EtudiantsMalades e)
        {
            System.err.println( "L'arbre choisi par l'utilisateur n'existe pas ou est déjà remarquable ! " +
                                "Comportement impossible : " + e.getMessage());
            return;
        }
        catch (NullPointerException e)
        {
            System.err.println("Le votant n'existe pas, le programme ne devrait pas arriver jusque là !");
            return;
        }

        System.out.println("Le vote a été effectué !");
    }
}
