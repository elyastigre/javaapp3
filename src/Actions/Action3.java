package Actions;

import data.Facture;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Action3 implements Action{
    public static void execute(){
        String name=util.LireClavier("Entrez votre nom de compagnie : ");
        String objet=util.LireClavier("Entrez le motif de cette facture : ");
        int prix;
        try {
            prix=Integer.parseInt(util.LireClavier("Entrez le prix de la facture : "));
        } catch (NumberFormatException e)
        {
            System.out.println("Prix invalide");
            return;
        }


        Date date = new Date();

        boolean erreur = true;

        while (erreur)
        {
            erreur = false;

            try {
                SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
                date = sd.parse(util.LireClavier("Entrez la date de facturation (format jj/mm/aaaa) : "));
            }
            catch (ParseException e)
            {
                erreur = true;
                System.out.println("Erreur, mauvais format, veuillez recommencer ! ");
            }
        }


        Facture f=new Facture(name,objet,date,prix);
        f.paiement();
    }
}
