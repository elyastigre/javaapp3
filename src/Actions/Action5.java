package Actions;

import data.Visites;
import data.arbre;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Action5 implements Action{

    /**
     * Execute l'action "Ajout d’une programmation de visite d’arbre remarquable et reception d’un compte-rendu pour
     * une visite programmee."
     * @param arbreList La liste des arbres
     */
    public static void execute(List<arbre> arbreList)
    {
        List<arbre> remarquableList = util.AfficheArbresRemarquables(arbreList);
        int choix = 0;

        try
        {
            choix = Integer.parseInt(util.LireClavier("Votre choix : "));
        }
        catch (NumberFormatException e)
        {
            choix = remarquableList.size() + 12;
        }

        if (choix > remarquableList.size())
        {
            System.out.println("Choix non valide");
        }
        else
        {
            if (!util.VerifieVisites(remarquableList.get(choix)))
            {
                System.out.println("Cet arbre fait déjà l'objet d'une visite");
            }
            else
            {
                int id = Integer.parseInt(util.LireClavier("Veuillez entrer votre ID : "));

                if (!data.membre.idExiste(id))
                {
                    System.out.println("Cet ID n'existe pas...");
                    return;
                }

                boolean erreur = true;
                Date dateVisite = new Date();

                while (erreur)
                {
                    erreur = false;
                    try
                    {
                        SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
                        dateVisite = sd.parse(util.LireClavier("Veuillez entrer une date de format dd/MM/yyyy"));
                    }
                    catch (ParseException e)
                    {
                        erreur = true;
                        System.out.println("Erreur dans le format de la date, veuillez entrer une date jj/MM/aaaa");
                    }

                }

                util.PlanifieVisite(id, remarquableList.get(choix), dateVisite);
                System.out.println("Visite planifiée !");

                int nbVisites = util.VerifieNbVisites(id);

                if (nbVisites <= 1)
                {
                    Visites.defrayement(id);
                    System.out.println("La visite vous est défrayée");
                }
                else
                {
                    System.out.println( "Ce n'est pas la seule visite que vous avez prévue cette année, " +
                                        "elle ne vous sera pas défrayée");
                }
            }
        }

    }


}
