package Actions;

import DataBase.query;
import Exceptions.ArbreDejaExistant;
import Exceptions.EtudiantsMalades;
import Exceptions.MembreDoesntExist;
import Exceptions.UtilisateurMalade;
import data.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class util {
    public static String LireClavier(String message)
    {
        try {
            System.out.print(message);
            BufferedReader clavier =
                    new BufferedReader(new InputStreamReader(System.in));
            return clavier.readLine();
        }
        catch (Exception e) {
            return "erreur dans fonction lireClavier";
        }
    }

    /***********************************************
     *                  Action 1                   *
     ***********************************************/

    /**
     * Lance la procédure pour désinscrire un membre
     */
    static void desincription() {
        int id = Integer.parseInt(util.LireClavier("Veuillez entrer l'ID a désinscrire : "));

        try
        {
            membre.desinscrire(id);
            System.out.println("Le membre " + id + " a été désinscrit, ses informations sont supprimés");
        } catch (MembreDoesntExist e)
        {
            System.out.println(e.getMessage());
        }

    }

    /**
     * Lance la procédure pour désinscrire un membre
     */
    static void inscription() {
        String nom=util.LireClavier("Votre nom: ");
        String prenom=util.LireClavier("Votre prénom: ");
        String adresse=util.LireClavier("Votre adresse: ");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date bdayDate = new Date();
        boolean erreur = true;

        while (erreur)
        {
            erreur = false;
            try
            {
                bdayDate = simpleDateFormat.parse(util.LireClavier("Votre date de naissance (format jj/mm/aaaa) : "));
            }
            catch (ParseException e)
            {
                erreur = true;
                System.out.println("Mauvais format de date, veuillez recommencer");
            }
        }

        membre m=new membre(nom,prenom,bdayDate,adresse);
        Cotisation.paiementCotisation(m.getID());
        System.out.println("Vous êtes bien inscrit");


    }

    /***********************************************
     *                  Action 4                   *
     ***********************************************/

    /**
     * Ajoute un donnateur (demandé dans la fonction
     */
    static void ajouteDonnateur() {
        String nom=LireClavier("Entrez votre nom de donnateur : ");
        donnateur.addDonnateur(nom);
    }

    /**
     * Supprime un donnateur (demandé dans la fonction)
     */
    static void supprimeDonnateur() {
        int identifiant;

        try
        {
            identifiant=Integer.parseInt(LireClavier("Entrez l'identifiant du donnateur à supprimer : "));
        }
        catch (NumberFormatException e)
        {
            identifiant = -1;
        }
        donnateur.supprDonnateur(identifiant);
    }

    /***********************************************
     *                  Action 5                   *
     ***********************************************/


    /**
     * Compte le nombre de visites prévues par le membre "id"
     * @param id le membre
     * @return le nombre de visite prévue par ce membre
     */
    static int VerifieNbVisites(int id) {
        int i = 0;

        for (Visites v : Visites.getCalendrier())
        {
            if (v.getId() == id)
            {
                i += 1;
            }
        }
        return i;
    }

    /**
     *  Planifie une visite du membre "id" pour l'arbre remarquable "arbre" au jour "jour"
     * @param id    l'id du membre qui fait la visite
     * @param arbre l'arbre objet de la visite
     * @param dateVisite La date de la visite
     */
    protected static void PlanifieVisite(int id, arbre arbre, Date dateVisite) {
        Visites v = new Visites(id, arbre, dateVisite);
    }

    /**
     * Vérifie si des visites ont été prévues pour un arbre donné
     * @param arbre un arbre
     * @return true si aucune n'a été prévue (pas de problème) et false si une visité a déjà été prévue
     */
    protected static boolean VerifieVisites(arbre arbre) {
        for (Visites v : Visites.getCalendrier())
        {
            if (v.getArbre() == arbre)
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Affiche la liste des arbres remarquables contenues dans arbreList et les stocks dans une liste qu'elle retourne
     * @param arbreList Une liste
     * @return La liste des arbres remarquables de arbreList triés par Ancienneté
     */
    protected static List<arbre> AfficheArbresRemarquables(List<arbre> arbreList) {
        List<arbre> listRemarquable = query.TriParAnciennete(query.SelectRemarquable(arbreList));
        int i = 0;

        for (arbre a : listRemarquable)
        {
            System.out.println(i + " | " + a.toString());
            i = i+1;
        }

        return listRemarquable;
    }

    /***********************************************
     *                  Action 6                   *
     ***********************************************/

    /**
     * Affiche la liste des arbres NON remarquables contenues dans arbreList et les stocks dans une liste qu'elle retourne
     * @param arbreList Une liste
     * @return La liste des arbres NON remarquables de arbreList triés par nombres de voies
     */
    protected static List<arbre> AfficheArbresNonRemarquables(List<arbre> arbreList) {
        List<arbre> listNonRemarquable = query.TriParVoies(query.SelectNonRemarquable(arbreList));
        int i = 0;

        for (arbre a : listNonRemarquable)
        {
            System.out.println(i + " | " + a.toString());
            i = i+1;
        }

        return listNonRemarquable;
    }

    /***********************************************
     *                  Action 7                   *
     ***********************************************/

    /**
     * Ouvre la liste des choix pour l'action 7
     * @return le choix choisi par l'utilisateur
     */
    public static int ChoixAction7() {
        int choix;

        while(true)
        {
            System.out.println("""
                    
                    Notification reçue :
                        1 : Plantation d'un arbre
                        2 : Abattage d'un arbre
                        3 : Classification en arbre remarquable
                        0 : Retour
                   
                    """);

            try
            {
                choix = Integer.parseInt(LireClavier("Votre choix : "));

            } catch (NumberFormatException e)
            {
                choix = 4;
            }

            if (choix >= 0 && choix <= 3)
            {
                return choix;
            }
        }
    }

    /**
     * Crée un nouvel arbre et l'ajoute dans la liste des arbres avec l'aide de l'utilisateur !
     * @param arbreList la liste des arbres
     */
    public static void NotifPlantationArbre(List<arbre> arbreList) {
        boolean erreur;

        // Demander IDbase
        String idbase = "";

        do {
            try {
                erreur = false;
                idbase = LireClavier("Veuillez entrer l'ID de l'arbre (un nombre qui n'a pas été pris) : \n");
                Integer.parseInt(idbase);
                if (arbre.exist(arbreList, Integer.parseInt(idbase)))
                {
                    throw new ArbreDejaExistant(Integer.parseInt(idbase));
                }
            } catch(NumberFormatException e)
            {
                erreur = true;
            }
            catch (Exceptions.ArbreDejaExistant e)
            {
                System.out.println(e.getMessage());
                erreur = true;
            }
        } while (erreur);

        // Demander Type emplacement  / domanialite
        String typeemplacement = LireClavier("Veuillez entrer le type d'emplacement : \n ");
        String domanialite = LireClavier("Veuillez choisir la domanialite : \n");

        // Le reste
        String arrondissement = LireClavier("Veuillez entrer l'arrondissement : \n");
        String complementadresse = LireClavier("Veuillez entrer le complément d'adresse : \n");
        String numero = LireClavier("Veuillez entrer le numéro : \n");
        String adresse = LireClavier("Veuillez entrer l'adresse : \n");
        String idemplacement = LireClavier("Veuillez entrer l'id de l'emplacement : \n");
        String libellefrancais = LireClavier("Veuillez indiquer le libelle Français s'il vous plait : \n");
        String genre = LireClavier("Veuillez entrer le genre de l'arbre (ça existe ?) : \n");
        String espece = LireClavier("Veuillez entrer l'espece (en cash) de l'arbre : \n");
        String varieteoucultivar = LireClavier("Veuillez entrer le/la varieteoucultivar (quoi que ce soit d'ailleurs) : \n");

        // Le reste plus dur !

        String circonferenceencm = "";

        do {
            try {
                erreur = false;
                int x = Integer.parseInt(LireClavier("Veuillez entrer la circonférence en cm (un nombre svp) : \n"));
                circonferenceencm = "" + x;
            } catch(NumberFormatException e)
            {
                erreur = true;
                System.out.println("Erreur, recommencez !");
            }
        } while (erreur);


        String hauteurenm = "";

        do {
            try {
                erreur = false;
                int x = Integer.parseInt(LireClavier("Veuillez entrer la hauteur en m (un nombre svp) : \n"));
                hauteurenm = "" + x;
            } catch(NumberFormatException e)
            {
                erreur = true;
                System.out.println("Erreur, recommencez !");
            }
        } while (erreur);

        String stadedeveloppement = "";
        int choix = -1;
        do
        {
            try
            {
                erreur = false;
                System.out.println("""
                    Veuillez choisir le stade de développement de l'arbre :
                        1 : Jeune (Arbre)
                        2 : Jaune (Arbre)Adulte
                        3 : Adulte
                        4 : Mature
                    """);
                choix = Integer.parseInt(LireClavier("Votre choix : "));
            } catch (NumberFormatException e)
            {
                erreur = true;
                System.out.println("Erreur, recommencez !");
            }
        } while (erreur || choix > 4 || choix < 1);

        switch (choix) {
            case 1 -> stadedeveloppement = "Jeune (Arbre)";
            case 2 -> stadedeveloppement = "Jeune (Arbre)Adulte";
            case 3 -> stadedeveloppement = "Adulte";
            case 4 -> stadedeveloppement = "Mature";
        }


        String remarquable = "";

        do
        {
            try
            {
                erreur = false;
                System.out.println("""
                    L'arbre est il remarquable ?
                        1 : Oui
                        2 : Non
                    """);
                choix = Integer.parseInt(LireClavier("Votre choix : "));
            } catch (NumberFormatException e)
            {
                erreur = true;
                System.out.println("Erreur, recommencez !");
            }
        } while (erreur || choix > 2 || choix < 1);

        if (choix == 1 ) remarquable = "OUI";


        String geo_point_2d = "";
        do
        {
            try
            {
                erreur = false;
                geo_point_2d = "" + Integer.parseInt(LireClavier("Première coordonnée 2D (un nombre svp) : ")) +"," + Integer.parseInt(LireClavier("Deuxième coordonnée 2D (un nombre svp) : "));
            } catch (NumberFormatException e)
            {
                erreur = true;
                System.out.println("Erreur, recommencez !");
            }
        } while (erreur);

        // Creation de l'arbre !
        arbreList.add(new arbre(idbase, typeemplacement, domanialite, arrondissement,
                complementadresse,  numero,  adresse,  idemplacement,  libellefrancais,
                 genre,  espece,  varieteoucultivar,  circonferenceencm, hauteurenm,
                 stadedeveloppement,  remarquable,  geo_point_2d));

        System.out.println("L'arbre a été créé !!! YOUPI !");
    }

    /**
     * Reception de notification de la classification d'un arbre en arbre remarquable !
     * En cas d'erreur, sort de la fonction
     * @param arbreList la liste des arbres
     */
    public static void NotifClassificationRemarquable(List<arbre> arbreList) throws EtudiantsMalades{
        List<arbre> nonRemarquableList = AfficheArbresNonRemarquables(arbreList);

        int choix;
        try
        {
            choix = Integer.parseInt(util.LireClavier("Votre choix : "));
            if (choix >= nonRemarquableList.size() || choix < 0)
            {
                throw new Exceptions.UtilisateurMalade("Choix non valide...");
            }
        }
        catch (UtilisateurMalade e)
        {
            System.out.println("Choix non valide...");
            return;
        }

        if (query.SelectArbreID(arbreList, nonRemarquableList.get(choix).getID()) != null) {
            query.SelectArbreID(arbreList, nonRemarquableList.get(choix).getID()).setRemarquable();
            System.out.println("L'arbre a été classé remarquable !!!!!!!!!!");
        }
        else
            throw new EtudiantsMalades("Erreur dans la fonction NotifClassificationRemarquable(), la fonction query.SelectArbreID a return un null alors qu'elle ne devrait pas. Mauvais fonctionnement...");
    }

    /**
     * Notification d'abattage d'un arbre, supprime l'arbre de la liste arbreList avec son ID
     * @param arbreList la liste
     */
    public static void NotifAbattageArbre(List<arbre> arbreList) {
        try
        {
            int choix = Integer.parseInt(LireClavier("Veuillez entrer l'ID de l'arbre abattu : "));
            arbre abbatu = query.SelectArbreID(arbreList, choix);

            if (abbatu == null)
                throw new Exceptions.ArbreNexistePas(choix);

            arbreList.remove(abbatu);
            System.out.println("L'arbre a été abattu :'(");
        } catch(Exceptions.ArbreNexistePas e)
        {
            System.out.println(e.getMessage());
        }
    }

    /***************************************************
     *                  Bonne année !                  *
     ***************************************************/

    /**
     * Révocation des membres n’ayant pas réglé leur cotisation pour l’exercice écoulé.
     */
    public static void RevocationSiNonCotisation() {

        List<membre> membreList = membre.getList();
        for (int i = membreList.size()-1; i >= 0; i--)
        {
            membre m = membreList.get(i);

            if (!m.getAPaye())
            {
                System.out.println(m.getNom()+" a été supprimé");
                m.destroy();
            }

        }

        System.out.println("Fin des révocations");
    }

    /**
     * @param arbreList La liste des arbres
     */
    public static void ConstitutionListeArbresRemarquables(List<arbre> arbreList) {
        List<arbre> NonRemarquablesList = query.SelectNonRemarquable(arbreList);
        List<arbre> NonRemarquablesAvecVotes = query.SelectArbreVote(NonRemarquablesList);

        // Vous soulignerez le jeu de mot incroyable :)
        List<arbre> FinaListe = query.TriParVoies(query.TriParCirconference(query.TriParHauteur(NonRemarquablesAvecVotes)));

        FinaListe = FinaListe.stream().limit(5).collect(Collectors.toList());

        System.out.println( "-----------------------------------------------------------------------------------" +
                            "\nVoici les 5 nouveaux arbres remarquables : ");

        if (FinaListe.size() == 0)
        {
            System.out.println("Il n'y a aucun vote !");
            return;
        }

        for(int i = 0; i<FinaListe.size(); i++)
        {
            int arbreId = FinaListe.get(i).getID();
            for(int j = 0; j < arbreList.size(); j++)
            {
                if(arbreList.get(j).getID() == arbreId)
                {
                    arbreList.get(j).setRemarquable();
                    break;
                }
            }

            System.out.println(FinaListe.get(i).toString());
        }

        System.out.println("-----------------------------------------------------------------------------------");
    }



    /**
     * Genere le Rapport d'activite
     */
    public static void GenerationRapportActivite() {
        System.out.println("---------   RAPPORT D'ACTVITE   ---------\n");
        System.out.println("Le compte au début de l'année : "+ Budget.getCompteDebDeLAnnee());
        System.out.println("Le compte actuel : "+Budget.getCompte()+"\n");
        System.out.println("\t**\tDEPENSES\t***\t\n");
        System.out.println("Dépenses total : "+Budget.getDepense());

        // LES FACTURES

        System.out.println("\n Dont les factures : \n");
        List<Facture> listeF = Facture.getListeFacture();
        for (Facture f:listeF){
            if (f.getDate().compareTo(Budget.getDerniereAnneeBudgetaire()) > 0)
                System.out.println(f.toString());
        }


        // LES VISITES

        System.out.println("\n Dont les defrayements visites : "+Budget.getNbDefrayement()+" a "+Visites.getFrais()+" euros");
        System.out.println("\n Les visites : ");

        List<Visites> listeV=Visites.getCalendrier();
        for (Visites v:listeV){
            if (v.getDate().compareTo(Budget.getDerniereAnneeBudgetaire()) > 0)
                v.afficheVisite();
        }

        // LES RECETTES

        System.out.println("\t**\tRECETTES\t***\t\n");
        System.out.println("Recette total : "+Budget.getRecette());

        // LES COTISATIONS

        System.out.println("\n Dont les cotisations : \n");
        List<Cotisation> listeC = Cotisation.getListeCotisation();
        for (Cotisation c:listeC){
            if (c.getDate().compareTo(Budget.getDerniereAnneeBudgetaire()) > 0)
                System.out.println(c.toString());
        }

        // LES DONS

        System.out.println("\n Dont les dons : ");
        List<don> listD = don.getListeDons();
        for (don d : listD)
        {
            if (d.getDate().compareTo(Budget.getDerniereAnneeBudgetaire()) > 0)
                System.out.println(d.toString());
        }

        System.out.println("-------------------------------------------------------------------");
        System.out.println("FIN");
    }

    /**
     * Envoi et Reception des dons
     */
    public static void EnvoiDemandeDons() {
        List<donnateur> listDonnateur= donnateur.getListDonnateur();
        for(donnateur d:listDonnateur){
            System.out.println("Envoi de demande de dons à "+d.getNom());
            int montant;

            try
            {
                montant=Integer.parseInt(LireClavier("Entrez le montant de votre don (0 si vous ne voulez pas en faire un) : "));
            }
            catch (NumberFormatException e)
            {
                montant = 0;
            }

            if(montant>0){
                don.addDon(d.getNom(),montant);
                Budget.addRecette(montant);
                System.out.println("Vous avez fait un don de "+montant+" euros");
            }else{
                System.out.println("Vous n'avez pas fait de don. ");
            }
        }
        System.out.println("\n Toutes les demandes de dons ont été envoyées et reçues");
    }

    public static void electionNouveauPresident() {
        boolean erreur = true;

        if (membre.getList().size() == 0) {
            System.out.println("Il n'y a plus de membre dans l'association, Game OVER !");
            System.exit(0);
        }

        while (erreur)
        {
            erreur = false;

            int choix;

            try
            {
                choix = Integer.parseInt(util.LireClavier("Veuillez entrer l'ID du nouveau président (l'ancien n'est plus là) : "));
            } catch (NumberFormatException e)
            {
                choix = -1;
            }

            if (!membre.idExiste(choix))
            {
                erreur = true;
                System.out.println("ID non existant");
            }
            else
            {
                membre.setPresident(choix);
            }
        }
    }
}
