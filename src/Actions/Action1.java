package Actions;

public class Action1 implements Action{

    public static void execute() throws Exceptions.EtudiantsMalades {
        AfficheChoix();
        int choix;

        try
        {
            choix = Integer.parseInt(util.LireClavier("Votre choix :"));
        }
        catch (NumberFormatException e)
        {
            choix = 6;
        }

        switch (choix) {
            case 1 -> util.inscription();
            case 2 -> util.desincription();
            default -> {
            }
        }

    }

    public static void AfficheChoix() {
        System.out.println(
                """
                        Veuillez séléctionner une action :
                        1 : Inscription d'un membre
                        2 : Désincription d'un membre
                        0 : Revenir au menu principal
                        """
        );
    }
}
