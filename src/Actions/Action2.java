package Actions;

import data.Cotisation;

public class Action2 implements Action{
    public static void execute(){
        int id;

        try
        {
            id=Integer.parseInt(util.LireClavier("Entrez votre identifiant : "));
        }
        catch (NumberFormatException e)
        {
            id = -1;
        }

        Cotisation.paiementCotisation(id);
    }
}
