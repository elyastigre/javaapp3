package Actions;

import data.arbre;

import java.util.List;

public class Action7 implements Action {

    public static void execute(List<arbre> arbreList) throws Exceptions.EtudiantsMalades
    {
        int choix = util.ChoixAction7();

        switch (choix)
        {
            case 1:
                util.NotifPlantationArbre(arbreList);
                break;
            case 2:
                util.NotifAbattageArbre(arbreList);
                break;
            case 3:
                util.NotifClassificationRemarquable(arbreList);
                break;
            case 0:
                break;
        }
    }
}
