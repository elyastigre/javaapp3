package data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Visites {
    private final int idVisite;
    private final int idMembre;
    private final arbre arbreRemarquable;
    private final Date dateVisite;

    private static final int FRAIS=10;
    private static int seq_id=1;
    private static List<Visites> Calendrier = new ArrayList<>();

    public Visites(int id, arbre a, Date dateVisite)
    {
        idVisite=seq_id;
        seq_id++;
        idMembre = id;
        arbreRemarquable = a;
        this.dateVisite = dateVisite;

        Calendrier.add(this);
    }

    /**
     * @return Le Calendrier de l'ensemble des visites
     */
    public static List<Visites> getCalendrier()
    {
        return Calendrier;
    }

    /**
     * Comptabilise un Defrayement (peut être ajouter un paramètre somme, ou alors seulement ajouter une somme fixe
     *                              à la sortie du budget)
     */
    public static void defrayement(int ID) {
        if(Budget.possibleDePayer(FRAIS)) {
            Budget.addDepenses(FRAIS);
            Budget.addDefrayement();
            System.out.println("Le membre " + ID + " a été remboursé de " + FRAIS + " euros pour sa visite");
        }else{
            System.out.println("Impossible de rembourser le membre, pas assez d'argent sur le compte");
        }
    }

    public static int getFrais() { return FRAIS;
    }

    /**
     * @return l'arbre de la visite
     */
    public arbre getArbre() {
        return arbreRemarquable;
    }

    /**
     * @return l'id de la personne en charge de la visite
     */
    public int getId() {
        return idMembre;
    }

    public void afficheVisite() //
    {
        System.out.print("La visite : " + idVisite + ", gérée par le membre " +
                ((membre.chercheMembre(idMembre)==null)? "???": membre.chercheMembre(idMembre).getNom()) +
                "sur l'arbre remarquable : " + arbreRemarquable.getID() + " aura lieu le " + dateVisite.toString()
        );
    }

    /**
     * Donne la date de la visite
     * @return la date de la visite
     */
    public Date getDate() {
        return dateVisite;
    }
}
