package data;

import java.util.Date;

public class Budget {

    private static int compte=20000;
    private static int recette=0;
    private static int depenses=0;
    private static int nbDefrayement=0;
    private static int compteDebDeLAnnee=20000;

    private static Date derniereAnneeBudgetaire;

    //SETTEURS

    /**
     * Ajoute le montant à celui des recettes de l'année et l'ajoute sur le compte
     * @param montant, le montant reçu
     */
    public static void addRecette(int montant){
        recette=recette+montant;
        compte=compte+montant;
    }

    /**
     * Ajoute le montant aux dépenses de l'année et enlève les sous du compte
     * @param montant, montant à payer
     */
    public static void addDepenses(int montant) {
        depenses=depenses+montant;
        compte=compte-montant;
    }

    /**
     * Ajoute un defrayement
     */
    public static void addDefrayement(){
        nbDefrayement++;
    }

    /**
     * indique si on peut payé la facture
     * @return si après paiement le solde est positif
     */
    public static boolean possibleDePayer(int montant){
        return montant < Budget.getCompte();
    }

    /**
     * Met à jour la date à laquelle le dernier exercice budgetaire a été lancé
     * @param a, la date du lancement de l'année budgétaire
     */
    public static void setDerniereAneeBudgetaire(Date a) { derniereAnneeBudgetaire = a;}

    //GETTEURS

    /**
     * Donne la valeur du compte compte courant
     * @return compte courant
     */
    public static int getCompte(){
        return compte;
    }

    /**
     * donne le compte de debut d'annee
     * @return compteDebDeLAnnee
     */
    public static int getCompteDebDeLAnnee(){return compteDebDeLAnnee;}

    /**
     * Donne la depense
     * @return depenses
     */
    public static int getDepense() {return depenses;}

    /**
     * Donne le nombre de defrayement
     * @return nbDefrayement
     */
    public static int getNbDefrayement() { return nbDefrayement;}

    /**
     * Donne la recette
     * @return recette
     */
    public static int getRecette() { return recette;}

    /**
     * Donne la date de la derniere fin d'exercice budgetaire
     * @return derniereAneeBudgetaire
     */
    public static Date getDerniereAnneeBudgetaire() {return derniereAnneeBudgetaire;}

    /**
     * Verifie si la partie doit se terminer (en fin d'année si le budget n'est plus suffisant)
     */
    public static void verifieGameOver() {
        if (compte <= 0)
            gameOver();
    }

    /**
     * Met fin à la partie !
     */
    public static void gameOver() {
        System.out.println("Game Over ! Les hussiers de justice arrivent !");
        System.exit(0);
    }

    /**
     * Met a jour le compte de debut d'annee
     */
    public static void setCompteDebDeLAnnee() {
        compteDebDeLAnnee = compte;
    }

    /**
     * Reset le nombre de Deffrayement et le set à 0
     */
    public static void resetNbDeffrayement() {
        nbDefrayement=0;
    }
}
