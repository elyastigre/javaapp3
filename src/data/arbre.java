package data;

import java.util.List;
import java.util.Objects;

public class arbre {
    private final String  idbase;
    private final String typeemplacement;
    private final String domanialite;
    private final String arrondissement;
    private final String complementadresse;
    private final String numero;
    private final String adresse;
    private final String idemplacement;
    private final String libellefrancais;
    private final String genre;
    private final String espece;
    private final String varieteoucultivar;
    private final String circonferenceencm;
    private final String hauteurenm;
    private final String stadedeveloppement;
    private String remarquable;
    private final String geo_point_2d;

    private int nbVoies;

    public arbre(String Idbase, String Typeemplacement, String Domanialite, String Arrondissement,
                 String Complementadresse, String Numero, String Adresse, String Idemplacement, String Libellefrancais,
                 String Genre, String Espece, String Varieteoucultivar, String Circonferenceencm, String Hauteurenm,
                 String Stadedeveloppement, String Remarquable, String Geo_point_2d)
    {
                idbase = Idbase;
                typeemplacement = Typeemplacement;
                domanialite = Domanialite;
                arrondissement = Arrondissement;
                complementadresse = Complementadresse;
                numero = Numero;
                adresse = Adresse;
                idemplacement = Idemplacement;
                libellefrancais = Libellefrancais;
                genre = Genre;
                espece = Espece;
                varieteoucultivar = Varieteoucultivar;
                circonferenceencm = Circonferenceencm;
                hauteurenm = Hauteurenm;
                stadedeveloppement = Stadedeveloppement;
                remarquable = Remarquable;
                geo_point_2d = Geo_point_2d;

                nbVoies = 0;
    }

    /**
     * Compare l'anciennete de deux arbres
     * @param arbre le premier arbre
     * @param arbre1 le deuxieme arbre
     * @return true si arbre est plus vieux que arbre1, false sinon
     */
    public static boolean compareAnciennete(arbre arbre, arbre arbre1) {
        if (Objects.equals(arbre.getAnciennete(), "Mature"))
        {
            return true;
        }
        else if (Objects.equals(arbre.getAnciennete(), "Adulte"))
        {
            return (!(Objects.equals(arbre1.getAnciennete(), "Mature")));
        }
        else if (Objects.equals(arbre.getAnciennete(), "Jeune (arbre)Adulte"))
        {
            return (!(Objects.equals(arbre1.getAnciennete(), "Mature")) && !(Objects.equals(arbre1.getAnciennete(), "Adulte")));
        }
        else if (Objects.equals(arbre.getAnciennete(), "Jeune (arbre)"))
        {
            return !(Objects.equals(arbre1.getAnciennete(), "Mature")) && !(Objects.equals(arbre1.getAnciennete(), "Adulte")) && !(Objects.equals(arbre1.getAnciennete(), "Jeune (arbre)Adulte"));
        }
        else
        {
            return Objects.equals(arbre1.getAnciennete(), "");
        }
    }

    @Override
    public String toString()
    {
        return  idbase + "; " + typeemplacement + "; " + domanialite +
                arrondissement + "; " + complementadresse + "; " + numero +
                adresse + "; " + idemplacement + "; " + libellefrancais +
                genre + "; " + espece + "; " + varieteoucultivar +
                circonferenceencm + "; " + hauteurenm + "; " + stadedeveloppement +
                remarquable + "; " + geo_point_2d;
    }

    /**
     * @return true si l'arbre est remarquable
     */
    public boolean isRemarquable() {
        return remarquable.equals("OUI");
    }

    /**
     * @return le stade de développement de l'arbre
     */
    public String getAnciennete()
    {
        return stadedeveloppement;
    }

    /**
     * Lorsqu'un membre vote pour cet arbre, augmente son compteur de voie de 1 si il n'est pas déjà remarquable
     * @return true si l'arbre n'était pas remarquable, false sinon
     */
    public boolean Vote()
    {
        if (isRemarquable()) return false;
        else
        {
            nbVoies++;
            return true;
        }
    }

    /**
     * @return idbase
     */
    public int getID() {
        return Integer.parseInt(idbase);
    }

    /**
     * @return nbVoies
     */
    public int getNbVoies() { return nbVoies;}

    /**
     * Rend l'arbre remarquable
     */
    public void setRemarquable() { remarquable = "OUI"; }

    /**
     * @return la hauteur en m
     */
    public float getHauteur() { return Integer.parseInt(hauteurenm);}

    /**
     * @return la circonference en cm
     */
    public float getCirconference() { return Integer.parseInt(circonferenceencm);}

    /**
     * Cherche l'arbre d'Id "id" dans la liste "arbreList" et renvoie si l'arbre existe dans la liste ou non
     * @param arbreList La liste des arbres dans laquelle on cherche
     * @param id        L'ID de l'arbre que l'on cherche
     * @return          true si l'arbre existe, false sinon
     */
    public static boolean exist(List<arbre> arbreList, int id) {
        for (arbre a : arbreList)
        {
            if (a.getID() == id) return true;
        }
        return false;
    }
}
