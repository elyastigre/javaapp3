package data;

import DataBase.query;
import Exceptions.EtudiantsMalades;
import Exceptions.MembreDoesntExist;

import javax.security.auth.Destroyable;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

public class membre implements Destroyable {

    private int id;
    private String nom;
    private String prenom;
    private Date dateDeNaissance;
    private String adresse;
    private Date derniereInscription;
    private boolean aPaye;  // true si le membre a payé pendant l'exercice budgetaire actuel, false sinon
    private List<Cotisation> cotisation;
    private List<Integer> voteArbreRemarquable; // La liste des arbres remarquables pour lequel il a voté

    private static int President = -1;
    private static List<membre> ListMembres = new ArrayList<>();
    private static int seq_id = 1;


    public membre(String nom, String fisrtname, Date naissance, String adresse) { //naissance doit être dans le format "jj/mm/aaaa"
        this.nom = nom;
        prenom=fisrtname;
        this.adresse=adresse;
        dateDeNaissance = naissance;


        aPaye=false;
        cotisation=new ArrayList<>();
        id = seq_id;
        seq_id++;

        voteArbreRemarquable = new ArrayList<>();

        ListMembres.add(this);
    }

    /**
     * Reset la valeur de tous les "aPaye" à false
     */
    public static void ResetAPaye() {
        for (membre m : ListMembres)
        {
            m.setAPaye(false);
        }
    }

    /**
     * Desinscrit le mmebre à l'id mis en paramètre
     * @param id l'id du membre à desinscrire
     * @throws MembreDoesntExist Si le membre n'existe pas !
     */
    public static void desinscrire(int id) throws MembreDoesntExist {

        if (!membre.idExiste(id)) throw new MembreDoesntExist(id);

        chercheMembre(id).destroy();
    }

    /**
     * Donne l'id du président
     * @return l'id de président
     */
    public static int getPresident() {return President;}

    /**
     * Fait voter le membre pour l'arbre ID de la liste d'arbre mis en paramètres
     * @param arbreList     la liste des arbres qui peuvent être votés
     * @param id            l'id de l'arbre voté
     */

    public void Vote(List<arbre> arbreList, int id) throws EtudiantsMalades {
        arbre arbreID = query.SelectArbreID(arbreList, id);
        if (arbreID == null)
            throw new EtudiantsMalades("Cet arbre n'existe pas");
        else
        {
            if (arbreID.Vote())
                if  (voteArbreRemarquable.size() > 4)
                {
                    voteArbreRemarquable.remove(0);
                    voteArbreRemarquable.add(id);
                }
                else
                    voteArbreRemarquable.add(id);
            else
                throw new EtudiantsMalades("Cet arbre est déjà remarquable");
        }

    }

    /**
     * @param id un ID dont l'on souhaite vérifier l'éxistence
     * @return true si il existe dans la liste des membres, false sinon.
     */
    public static boolean idExiste(int id)
    {
        for (membre m : ListMembres)
        {
            if (m.getID() == id) return true;
        }
        return false;
    }

    /**
     * On suppose ici qu'on a vérifié avant que l'id existe
     * @param id du membre cherché
     * @return le membre correspondant
     */
    public static membre chercheMembre(int id){
        if (idExiste(id)){
            for (membre m : ListMembres) {
                if (m.getID() == id) {
                    return m;
                }
            }
        }
        return null;

    }

    // SETTERS

    /**
     * Determine l'id du nouveau president
     * @param id l'id du nouveau president
     */
    public static void setPresident(int id)
    {
        President = id;
    }

    /**
     * Determine le nouveau président
     * @param m un membre
     */
    public static void setPresident(membre m)
    {
        President = m.getID();
    }

    /**
     * Supprime le membre de la liste des membres, et supprime ses informations
     */
    public void destroy()
    {
        ListMembres.remove(this);

        id = 0;
        nom = "";
        prenom = "";
        adresse = "";
        cotisation = null;
        dateDeNaissance = null;
        derniereInscription = null;
        voteArbreRemarquable = null;
        aPaye = false;
    }

    /**
     * Met le champ Payé à a
     * @param a la nouvelle valeur de aPaye
     */
    public void setAPaye(boolean a){
        aPaye=a;
    }

    /**
     * Ajoute une cotisation au membre
     * @param c la cotisation que le membre à payer
     */
    public void addCostisation(Cotisation c){
        cotisation.add(c);
    }

    /**
     * Met à jour la date de dernière inscription
     */
    public void setDerniereInscription(){
        derniereInscription=new Date();
    }

    // GETTERS

    /**
     * Getter ID
     * @return id
     */
    public int getID() { return id; }

    /**
     * @return la liste des membres
     */
    public static List<membre> getList()
    {
        return ListMembres;
    }

    public String getNom(){
        return nom;
    }

    public String getPrenom(){
        return prenom;
    }


    /**
     * @return Si le membre a payé sa cotisation annuelle
     */
    public boolean getAPaye() { return aPaye;}

    public String toString(){
        return getID()+" : "+getNom()+" "+getPrenom();
    }

}
