package data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class don {
    private final int id;
    private final int montant;
    private final Date d;
    private final String donnateur;
    private static int seq_id=1;
    private static List<don> listDon=new ArrayList<>();

    public don(String nom, int montant){
        id=seq_id;
        seq_id++;
        d=new Date();
        this.montant=montant;
        donnateur=nom;

        listDon.add(this);

    }

    /**
     * Crée un nouveau don et l'ajoute àla liste
     * @param nom le nom du donnateur
     * @param montant le montant reçu
     */
    public static void addDon(String nom, int montant){
        don d=new don(nom,montant);
    }

    public static List<don> getListeDons() {
        return listDon;
    }

    /**
     * Donne la chaîne de caractère représentant un don pour l'afficher
     * @return Une string représentant un don
     */
    public String toString(){
        return "Don de "+getDonnateur()+" d'un montant de "+getMontant()+" euros";
    }

    /**
     * Recupère le montant du don
     * @return le montant du don
     */
    public int getMontant() { return montant;
    }

    /**
     * Recupère le nom du donnateur
     * @return Chaîne de caractère avec le nom du donnateur
     */
    public String getDonnateur() { return donnateur;
    }

    /**
     * retourne la date à laquelle le don a été effectué
     * @return La date
     */
    public Date getDate(){return d;}
}
