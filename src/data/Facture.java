package data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Facture {
    private final int id;
    private final String nomCompagnie;
    private final String motif;
    private final int montant;
    private final Date date;
    private boolean estPaye;

    private static int seq_id=1;
    private static List<Facture> listeFacture=new ArrayList<>();

    public Facture(String name, String objet,Date dateDeFacturation, int prix){
        id=seq_id;
        seq_id++;

        this.date = dateDeFacturation;

        nomCompagnie=name;
        motif=objet;
        montant=prix;
        estPaye=false;
        listeFacture.add(this);

    }

    /**
     * Paiement de facture si l'argent est disponible sinon met fin à la partie
     */
    public void paiement(){
        if(Budget.possibleDePayer(montant)){
            Budget.addDepenses(montant);
            setEstPaye();
            System.out.println("Votre facture de "+montant+" euros a été payée.");
        }else{
            System.out.println("Votre facture n'a pas pu être payé.");
            Budget.gameOver();
        }
    }

    //SETTEURS
    /**
     * Met estPayé a vrai
     */
    public void setEstPaye(){
        estPaye=true;
    }

    /**
     * Recupère le nom de la compagnie
     * @return String contenant le nom de la compagnie
     */
    public String getNomCompagnie(){return nomCompagnie;}

    /**
     * Recupère le motif de la facture
     * @return String du motif de la facture
     */
    public String getMotif(){return motif;}

    /**
     * Recupère le montant de la facture
     * @return le montant de la facture
     */
    public int getMontant(){return montant;}

    /**
     * Recupère un booléan indiquant si la facture est payée
     * @return vrai si la facture est payé, faux sinon
     */
    public boolean getEstPaye(){return estPaye;}

    /**
     * Retourne une String pour afficher une facture
     * @return une String représentant une facture
     */
    public String toString(){
        return "La facture de "+getNomCompagnie()+" pour "+getMotif()+" d'une valeur de "+getMontant()+" euros est "+((getEstPaye())?"payée":"impayée");
    }

    /**
     * Recupère la liste des factures
     * @return la liste des factures
     */
    public static List<Facture> getListeFacture(){
        return listeFacture;
    }


    public Date getDate() { return date; }
}
