package data;

import javax.security.auth.Destroyable;
import java.util.ArrayList;
import java.util.List;

public class donnateur implements Destroyable {
    private String nom;
    private int id;
    private static int seq_id=1;
    private static List<donnateur> listDonnateur=new ArrayList<>();

    public donnateur(String name){
        nom=name;
        id=seq_id;
        seq_id++;
        listDonnateur.add(this);
    }

    /**
     * Ajoute un nouveau donnateur
     * @param name le nom du donnateur
     */
    public static void addDonnateur(String name) {
        donnateur d=new donnateur(name);
        System.out.println("Votre id de donnateur est : "+d.getID());
    }

    /**
     * Recupère la liste d'un donnateur
     * @return la liste des donnateurs
     */
    public static List<donnateur> getListDonnateur() {
        return listDonnateur;
    }

    /**
     * Recupère le nom de donnateur
     * @return la String du nom du donnateur
     */
    public String getNom() {
        return nom;
    }

    /**
     * Recupère l'ID du donnateur
     * @return l'indentifiant du donnateur
     */
    public int getID() {
        return id;
    }

    /**
     * Supprime un donnateur de la liste
     * @param identifiant l'identifiant du donnateur à supprimer
     */
    public static void supprDonnateur(int identifiant) {
        if(exist(identifiant)){
            donnateur d=chercheDonnateur(identifiant);
            if(d!=null){
                d.destroy();
                System.out.println("Donnateur supprimé");
            }else{
                System.out.println("Donnateur inexistant");
            }
        }else{
            System.out.println("Donnateur inexistant");
        }

    }

    /**
     * Regarde si un donnateur existe
     * @param ID l'identifiant du donnateur
     * @return vrai si le donnateur est dans notre liste de donnateur, sinon faux
     */
    public static boolean exist(int ID){
        for (donnateur d:listDonnateur){
            if(d.getID()==ID){
                return true;
            }
        }
        return false;
    }

    /**
     * Cherche un donnateur dans la liste de donnateur
     * @param id l'identifiant du donnateur
     * @return l'objet donnateur correspondant ou null s'il n'a pas été trouvé
     */
    public static donnateur chercheDonnateur(int id){
        for (donnateur d: listDonnateur) {
            if (d.getID() == id) {
                return d;
            }
        }
        return null;

    }

    /**
     * Détruit les informations d'un donnateur et l'enlève de la liste
     */
    public void destroy()
    {
        listDonnateur.remove(this);

        id = -1;
        nom = "";
    }
}
