package data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Cotisation {
    private final int id;
    private final int idMembre;
    private final Date date;
    private static final int MONTANT=25;
    private static List<Cotisation> listCotisation=new ArrayList<>();
    private static int seq_id = 1;

    public Cotisation(int MembreID){
        id=seq_id;
        seq_id++;
        idMembre=MembreID;
        date=new Date();
        listCotisation.add(this);
    }

    /**
     * Paiement d'une cotisation si le membre existe
     * @param MembreID, l'identifiant du membre qui paiela cotisation
     */
    public static void paiementCotisation(int MembreID){

        if(membre.idExiste(MembreID)){
            Cotisation c=new Cotisation(MembreID);
            membre m=membre.chercheMembre(MembreID);

            if(m!=null) {
                m.addCostisation(c);
                m.setAPaye(true);
                m.setDerniereInscription();
                Budget.addRecette(MONTANT);
                System.out.println("Vous avez payé votre cotisation de " + MONTANT + " euros. L'ID de votre cotisation est le " + c.getId());
            }else{
                System.out.println("Membre inexistant");
            }

        }else{
            System.out.println("Membre inexistant");
        }

    }

    /**
     * Recupère la liste des cotisations
     * @return la liste des cotisations
     */
    public static List<Cotisation> getListeCotisation() { return listCotisation;
    }

    /**
     * Récupère l'identifiant de la cotisation
     * @return l'identifiant de la cotisation
     */
    public int getId(){
        return id;
    }

    /**
     *  Renvoie une chaîne de caractère pour l'affichage d'une cotisation
     * @return String représentant une cotisation
     */
    public String toString(){
        return "Le membre "+getIdMembre()+" a payé sa cotisation de "+getMontant()+" euros";
    }

    /**
     * Récupère le montant des cotisations
     * @return le prix d'une cotisation
     */
    private int getMontant() { return MONTANT;
    }

    /**
     * Récupère l'id du membre qui a payé la cotisation
     * @return l'identifiant du membre
     */
    private int getIdMembre() { return idMembre;
    }

    /**
     * Recupère la date de paiement de la cotisation
     * @return une date
     */
    public Date getDate() {
        return this.date;
    }
}
