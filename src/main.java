import Actions.*;
import Exceptions.EtudiantsMalades;
import data.Budget;
import data.arbre;
import DataBase.query;
import data.donnateur;
import data.membre;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class main {

    public static void AfficheOptions()
    {
        System.out.println(
                """
                        
                        Veuillez séléctionner une action :
                        1 : Inscription/Désinscription d'un membre
                        2 : Recette de la cotisation d'un membre
                        3 : Paiement d'une facture reçue
                        4 : Ajout d'un nouveau donateur/Supression d'un donateur
                        5 : Ajout d'une programmation de visite d'arbre remarquable et réception d'un compte rendu pour une visite programmée
                        6 : Vote d'un membre en faveur de la reconnaissance d'un arbre remarquable
                        7 : Réception de notifications concernant la plantation/abattage d'arbres/classification en arbre remarquable
                        8 : Fin d'un exercice budgetaire
                        0 : Quitter
                        
                        """
        );
    }



    public static void main(String[] args) throws EtudiantsMalades{

        // Situation initiale
        SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");

        try
        {
            Date a = sd.parse("05/08/2001");

            Budget.setDerniereAneeBudgetaire(a);

        } catch (ParseException e)
        {
            throw new EtudiantsMalades("Si cette erreur arrive, c'est définitivemetn la CoronaVirus");
        }

        Date solene;
        Date elyas;
        Date lichtie;
        try
        {
            solene = sd.parse("05/08/2001");
            elyas = sd.parse("09/06/2001");
            lichtie = sd.parse("25/12/2018");
        } catch (ParseException e)
        {
            throw new EtudiantsMalades("Si cette erreur arrive un jour, on a vraiment un problème...");
        }


        membre Solene = new membre("Hirles", "Solene", solene, "Chez Solene");
        membre Elyas = new membre("Tigre", "Elyas", elyas, "Chez Elyas");
        membre Lichtie = new membre("Tigre", "Lichtie", lichtie, "Chez Elyas ou dans son sac");
        // Le saviez vous ? Lichtie est la peluche Tigre que Elyas ramène à chaque fois !
        membre.setPresident(Lichtie);

        donnateur Pitchoune = new donnateur("Pitchoune");

        // Main

        List<arbre> arbreList = new ArrayList<>();

        try
        {
            arbreList = query.extract(args[0]);
        }
        catch (IOException e)
        {
            System.err.println("Problème dans la lecture du fichier");
        }

        int choix = -1;

        while (choix!=0)
        {
            AfficheOptions();
            try
            {
                choix = Integer.parseInt(util.LireClavier("Votre choix :"));
            } catch (NumberFormatException e)
            {
                choix = 14;
            }

            switch (choix) {
                case 1 -> Action1.execute();
                case 2 -> Action2.execute();
                case 3 -> Action3.execute();
                case 4 -> Action4.execute();
                case 5 -> Action5.execute(arbreList);
                case 6 -> Action6.execute(arbreList);
                case 7 -> Action7.execute(arbreList);
                case 8 -> FinExerciceBudgetaire.execute(arbreList);
                default -> {
                }
            }
        }
    }
}
